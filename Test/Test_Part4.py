from functools import reduce
''' Часть 4. Кирпичики функционального программирования '''

'''
Задача 1. Вспомним, что функция map(f, alist) отображает элементы списка
alist по некоторому правилу f, то есть:

  x1          x2  : ... :     xn    # элементы списка alist
   |           |  :     :      |
f(x1)       f(x2) : ... :   f(xn)   # результирующий список

Используя функцию map(), напишите функции для следующих ситуаций:
    - uppers(s) - принимает строку и возвращает строку в верхнем регистре
    - doubles(nums) - удваивает каждое число в списке nums
    - rubsToDollars(coins) - переводит рубли в доллары
'''
def uppers(s):
    return ''.join(map(lambda x: x.upper(), s))


def doubles(nums):
    return list(map(lambda x: x * 2, nums))


def rubsToDollars(coins):
    return list(map(lambda x: x / 55, coins))


'''
Задача 2. Используя функцию filter(), напишите функции для следующих ситуаций:
    - alphas(s) - сохраняет в строке только символы английского алфавита
    - rmChar(c, s) - удаляет из строки s все вхождения символа c
    - above(n, nums) - удаляет из списка nums все числа, которые меньше чем n
    - unequals(pairs) - удаляет из списка пар pairs все пары, в которых x == y.
      Список pairs выглядит следующим образом: [(x1, y1), (x2, y2), ...., (xn, yn)]
'''
def alphas(s):
    return ''.join(filter(lambda x: x >= 'A' and x <= 'z', s))

# print(alphas("qweQweПривет"))

def rmChar(c, s):
    return ''.join(filter(lambda x: x != c, s))

# print(rmChar('q', 'qweqweqwe'))

def above(n, nums):
    return list(filter(lambda x: x > n, nums))

# print(above(10, [11, 5, 6, 12]))

def unequals(pairs):
    return list(filter(lambda x: x[0] == x[1], pairs))

# print(unequals([(1, 4), (2, 2)]))

'''
Задача 3. Используя функцию reduce(), напишите функции для следующих ситуаций:
    - product(nums) - возвращает произведение чисел в списке nums
    - andRec(alist) - проверяет, являются ли все значения в списке alist истинными
    - concat(list_of_list) -  объединяет все списки в list_of_list в один список
    - rmChars(chars, str) - удаляет из строки str все символы, которые есть в списке chars
'''

def product(nums):
    return reduce(lambda x, y: x * y, nums)

# print(product([1, 2, 3]))

def andRec(alist):
    return reduce(lambda x, y: x and y, alist)
# print(andRec([0, 1, 0, 0]))

def concat(list_of_list):
    return reduce(lambda x, y: x + y, list_of_list)

# print(concat([[1, 2], [3, 5]]))

def rmChars(chars, strg):
    return reduce(lambda x, y: x if y in chars else x + y, strg)

print(rmChars(['a', 'b'], "qawb"))
