emptyIrlist = ()
def make_irlist(first, rest = emptyIrlist):
	return (first, rest)

def irlist_head(irl):
	return irl[0]

def irlist_rest(irl):
	return irl[1]

def irlist_len(irlist, acc = 0):
	if (irlist == emptyIrlist):
		return acc
	else:
		return irlist_len(irlist_rest(irlist), acc + 1)

def irlist_select(irlist, index):
	if (index == 0):
		return irlist_head(irlist)
	else:
		return irlist_select(irlist_rest(irlist), index - 1)
def irlist_prepend(irlist, value):
	return make_irlist(value, irlist)
def irlist_append(irlist, value):
	if (irlist == emptyIrlist):
		return make_irlist(value)
	else:
		return make_irlist(irlist_head(irlist), irlist_append(irlist_rest(irlist), value))

def reverse(irlist):
	if (irlist == emptyIrlist):
		return emptyIrlist
	else:
		return irlist_append(reverse(irlist_rest(irlist)), irlist_head(irlist))

def insert(irlist, value, index):
	if (index == 0):
		return make_irlist(value, irlist_rest(irlist))
	else:
		return irlist_prepend(insert(irlist_rest(irlist), value, index - 1), irlist_head(irlist))

mainIrList = make_irlist(1)
mainIrList = irlist_append(mainIrList, 5)
mainIrList = irlist_append(mainIrList, 10)
mainIrList = irlist_append(mainIrList, 6)
print(mainIrList)
mainIrList = reverse(mainIrList)
print(mainIrList)
mainIrList = insert(mainIrList, 8, 2)
print(mainIrList)