''' Часть 1. Задачи для разогрева '''

'''
Задача 1. Ниже представлена функция, которая возвращает значение 
Истина, если число является простым (простое число это то число,
которое делится на единицу и на себя), в противном случае Ложь.
'''
def is_prime(n):
    k = 2
    while k < n:
        if n % k == 0:
            return False
        k += 1
    return True

''' Используя функцию is_prime(n), напишите функцию nth_prime(n),
которая возвращает n-ое простое число, например:

>>> nth_prime(2)
3
>>> nth_prime(5)
11
'''
def nth_prime(n):
    counter = 0
    number = 1
    while (counter != n):
    	number+=1
    	if (is_prime(number)):
    		counter += 1

    return number
# print(nth_prime(2))

'''
Задача 2. В этой задаче требуется сгенерировать последовательность 
простых чисел до некоторого n-ого. Какой простой способ есть, 
чтобы сделать это?
'''
def primes_up_to_nth(n):
	primes = []
	for i in range(1, n + 1):
		if (is_prime(i)):
			primes.append(i)
	return primes

# print(primes_up_to_nth(20))

''' 
Задача 3. Последовательность Фибоначчи это одна из самых известных
последовательностей в математике, где каждое число является суммой
двух предыдущих: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ... 
Используя цикл while, напишите функцию которого находит n-ое число
Фибоначчи. Например:

>>> nth_fibo(4)
2
>>> nth_fibo(6)
5
'''
def nth_fibo(n):
    counter = 1
    fibNum = 0
    prevFibNum = 1
    while (counter != n):
    	fibNum, prevFibNum = fibNum+prevFibNum, fibNum
    	counter += 1
    return fibNum
print(nth_fibo(3))

