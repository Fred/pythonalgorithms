''' Часть 2. Функции высших порядков. Функции как аргументы '''

def square(x):
    return x * x

def double(x):
    return 2 * x


'''
Задача 1. Используя функции square(x) и double(x), напишите функции 
square_every_number(n) и double_every_number(n), которые возводят в 
квадрат или удваивают каждое натуральное число от 1 до n и выводят 
соответствующее значение на экран. Например:
>>> square_every_number(4)
1
4
9
16
>>> double_every_number(4)
2
4
6
8
'''
def square_every_number(n):
    for i in range(1, n + 1):
    	print(square(i))


def double_every_number(n):
    for i in range(1, n + 1):
    	print(double(i))



''' Вы могли заметить, что функции square_every_number(n) и double_every_number(n)
очень похожи. Мы можем их переписать в терминах функций высших порядков. 
Например, мы можем написать функцию every, которая принимает два аргумента:
функцию, которая будет применяться к некоторому значению и натуральное число n.
'''
def square_every_number(n):
    every(square, n)


def double_every_number(n):
    every(double, n)


''' 
Задача 2. Итак, ваша задача написать функцию every(func, n).
'''
def every(func, n):
    for i in range(1, n + 1):
    	print(func(i))


# square_every_number(4)
# double_every_number(4)

'''
Задача 3. Напишите функцию keep(cond, n), которая похожа на фунцию every(func, n),
за тем исключением, что она выводит на экран только те значения, которые
удовлетворяют условию cond. Например:
def is_even(x): return x % 2 == 0
>>> keep(is_even, 10)
2
4
6
8
10
'''
def keep(cond, n):
    for i in range(1, n + 1):
    	if (cond(i)):
    		print(i)

keep(lambda x: x % 2 == 0, 10)