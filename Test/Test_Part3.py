''' Часть 3. Функции высших порядков. Функции как возвращаемые значения 

Часто возникает задача, когда требуется написать функцию, которая 
возвращает функцию, которая в свою очередь делает что-то еще. Например:

def my_wicked_procedure(blah):
    def my_wicked_helper(more_blah):
        ...
    return my_wicked_helper

Это общая форма записи решения таких задач.
'''

''' 
Задача 1. Напишите функцию and_add_one(f), которая принимает функцию f как
аргумент и возвращает функцию, которая принимает один аргумент и делает 
тоже, что и функция f, за тем исключением, что прибавляет единицу к 
результату. Например:

>>> aao = and_add_one(square)
>>> aao(2)
5
>>> aao(3)
10
'''
def and_add_one(f):
    def inner(n):
    	return f(n) + 1
    return inner

# print(and_add_one(lambda x: x ** 2)(2))

'''
Задача 2. Напишите функцию and_add(f, n), которая очень похожа на 
функцию and_add_one, за тем исключением, что прибавляет к результату
работы функции f не единицу, а некоторое n.
'''
def and_add(f, n):
    def inner(nn):
    	return f(nn) + n
    return inner

# print(and_add(lambda x: x ** 2, 2)(2))

'''
Задача 3. Представьте, что уже написана функция accumulate, которая 
работает следующим образом:

>>> accumulate(add, 0, 5, identity)  # 0 + 1 + 2 + 3 + 4 + 5
15
>>> accumulate(add, 11, 5, identity) # 11 + 1 + 2 + 3 + 4 + 5
26
>>> accumulate(add, 11, 0, identity) # 11
11
>>> accumulate(add, 11, 3, square)   # 11 + 1^2 + 2^2 + 3^2
25

Ваша задача написать функцию lazy_accumulate, которая возвращает
функцию, принимающую некоторое целое значение m и возвращаующую
результат накопления (аккумуляции) первых n чисел, начинающихся
с 1, а затем добавляет к нему результат накопления последующих 
m чисел. Например:

>>> # (1 + 2 + 3 + 4 + 5) + (6 + 7 + 8 + 9 + 10)
>>> lazy_accumulate(add, 0, 5, identity)(5)
55
'''
def lazy_accumulate(f, start, n, term):
	res = start
	for i in range(1, n + 1):
		res = f(res, term(i))

	def cont_accumulate(m):
		resInner = 0
		for i in range(n + 1, n + m + 1):
			resInner = f(resInner, term(i))
		return resInner + res
	return cont_accumulate

print(lazy_accumulate(lambda x, y: x + y, 0, 5, lambda x: x)(5))



