def read_sudoku(filename):
	digits = [c for c in open(filename).read() if c in '123456789.']
	grid = group(digits, 9)
	return grid

def group(string, grid):
	normGrid = []
	normGrid.append([])
	n = 0
	for i in range(len(string)):
		normGrid[n].append(string[i])
		if ((i + 1) % grid == 0 and i != len(string) - 1):
			n+=1
			normGrid.append([])
	return normGrid

def display(grid):
	for i in range(len(grid)):
		if ((i % 3 == 0) and (i != 0) and (i != len(grid) - 1)):
			print("------+------+------")
		line = "";
		for j in range(len(grid[i])):
			line += grid[i][j] + " "
			if (((j+1) % 3 == 0) and (j != len(grid[i]) - 1)):
				line += "|"
		print(line)

def get_row(values, pos):
	row, col = pos
	return values[row]

def get_col(values, pos):
	row, col = pos
	return [values[i][col] for i in range(len(values))]

def get_block(values, pos):
	row, col = pos
	return [values[i][j] for i in range(row//3*3, row//3*3+3) for j in range(col//3*3, col//3*3+3)]

def find_empty_position(values):
	for i in range(len(values)):
		for j in range(len(values)):
			if(values[i][j] == '.'):
				return (i, j)


def find_possible_values(grid, pos):
	r = []
	for i in range(1, 10):
		if((str(i) not in get_block(grid, pos)) and (str(i) not in get_col(grid, pos)) and (str(i) not in get_row(grid, pos))):
			r.append(str(i))
	return r



def solve(values):
	pos = find_empty_position(values)
	if not pos:
		s = True
		return values #solved

	value = find_possible_values(values, pos)
	if not value:
		return None
	for i in value:
		values[ pos[0] ][ pos[1] ] = str(i)
		solution = solve(values)
		if not solution:
			values[ pos[0] ][ pos[1] ] = '.'
		else:
			return solution


def check_solution(solution):
	for i in range(0, 9):
		for j in range(0, 9):
			row = get_row(solution, (i, j))
			col = get_col(solution, (i, j))
			block = get_block(solution, (i, j))
			for k in range(1, 10):
				if((str(k) not in row) or (str(k) not in col) or (str(k) not in block)):
					return False
	return True
	
sudoku = read_sudoku('sudoku.txt')	
display(sudoku)

sol = solve(sudoku)
display(sol)
