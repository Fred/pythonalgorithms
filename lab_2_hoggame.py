from random import randint

def make_fair_dice(sides):
	assert type(sides) == int and sides >= 1, 'Illegal value for sides'
	pass

	def die():
		return randint(1, sides)
	return die

# 4-сторонний и 6-сторонний кубики.
four_sided_die = make_fair_dice(4)
six_sided_die = make_fair_dice(6)

def draw_number(n, dot = "*"):
    def one():
    	string = ' ------- \n'
    	string += '|       | \n'
    	string += '|   '+dot+'   | \n'
    	string += '|       | \n'
    	string += ' ------- \n'
    	return string
    def two():
    	string = ' ------- \n'
    	string += '|     '+dot+' | \n'
    	string += '|       | \n'
    	string += '| '+dot+'     | \n'
    	string += ' ------- \n'
    	return string
    def three():
    	string = ' ------- \n'
    	string += '|     '+dot+' | \n'
    	string += '|   '+dot+'   | \n'
    	string += '| '+dot+'     | \n'
    	string += ' ------- \n'
    	return string
    def four():
    	string = ' ------- \n'
    	string += '| '+dot+'   '+dot+' | \n'
    	string += '|       | \n'
    	string += '| '+dot+'   '+dot+' | \n'
    	string += ' ------- \n'
    	return string
    def five():
    	string = ' ------- \n'
    	string += '| '+dot+'   '+dot+' | \n'
    	string += '|   '+dot+'   | \n'
    	string += '| '+dot+'   '+dot+' | \n'
    	string += ' ------- \n'
    	return string
    def six():
    	string = ' ------- \n'
    	string += '| '+dot+'   '+dot+' | \n'
    	string += '| '+dot+'   '+dot+' | \n'
    	string += '| '+dot+'   '+dot+' | \n'
    	string += ' ------- \n'
    	return string
    if n == 1:
    	return one()
    elif n == 2:
    	return two()
    elif n == 3:
    	return three()
    elif n == 4:
    	return four()
    elif n == 5:
    	return five()
    else: return six()


def roll_dice(num_rolls, dice = six_sided_die):
	assert type(num_rolls) == int, 'num_rolls must be an integer.'
	assert num_rolls > 0, 'Must roll at least once.'
	pass

	res = 0
	for i in range(0, num_rolls):
		roll = dice()
		print(draw_number(roll))
		if (roll == 1):
			return roll
		else:
			res = res + roll
	
	return res

def take_turn(num_rolls,  opponent_score, dice=six_sided_die):
	assert type(num_rolls) == int, 'num_rolls must be an integer.'
	assert num_rolls >= 0, 'Cannot roll a negative number of dice.'
	assert num_rolls <= 10, 'Cannot roll more than 10 dice.'
	assert opponent_score < 100, 'The game should be over.'

	pass

def num_allowed_dice(score, opponent_score):
	return 1 if ((score + opponent_score) % 10 == 7) else 10


def select_dice(score, opponent_score):
	return four_sided_die if ((score + opponent_score) % 7 == 0) else six_sided_die