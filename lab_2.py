# worstCase = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
# bestCase = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
def shellSort(aList): # worst: O(n ^ 2) best: O(n * log^2(n))
	gap = len(aList) // 2
	while (gap >= 1):
		for i in range(gap, len(aList)):
			tmp = aList[i]
			j = i
			while (j >= gap and aList[j - gap] > tmp):
				aList[j] = aList[j - gap]
				j-=gap
			aList[j] = tmp
		gap = gap // 2
	return aList

# Худший и лучший вход зависит выбора элемента pivot
def quickSort(aList): # worst: O(n ^ 2). best: O(n*log(n))
	import random
	less = []
	equal = []
	more = []
	if (len(aList) <= 1):
		return aList
	else:
		# pivot = (max(aList) + min(aList)) // 2
		# pivot = aList[random.randint(0, len(aList) - 1)]
		pivot = aList[len(aList) // 2]
		for i in aList:
			if (i < pivot):
				less.append(i)
			if (i > pivot):
				more.append(i)
			if (i == pivot):
				equal.append(i)
		return quickSort(less) + equal + quickSort(more)

def mergeSort(aList): # O(n*log(n))
	if (len(aList) > 1):
		mid = len(aList) // 2
		lh = mergeSort(aList[:mid])
		rh = mergeSort(aList[mid:])

		i = 0
		j = 0
		k = 0

		while (i < len(lh) and j < len(rh)):
			if (lh[i] < rh[j]):
				aList[k] = lh[i]
				i+=1
			else:
				aList[k] = rh[j]
				j+=1
			k+=1

		while (i < len(lh)):
			aList[k] = lh[i]
			i+=1
			k+=1

		while (j < len(rh)):
			aList[k] = rh[j]
			j+=1
			k+=1



	return aList

def plotTimeComplexity(sortAlg, max_size):
	import random
	import time
	import matplotlib.pyplot as plt

	times = []
	sizes = []

	for size in range(max_size):
		vals = generateRandomNums(size)
		start_time = time.time()
		sortAlg(vals)
		stop_time = time.time()
		times.append(stop_time - start_time)
		sizes.append(size)

	plt.plot(sizes, times, "bo")
	plt.show()



def generateRandomNums(n, start = 0, stop = 100):
	import random
	return [random.randint(start, stop) for r in range(n)];

# arr = [4, 10, 2, 6, 7, 1, 3]
# print(shellSort(arr))
# arr = [4, 10, 2, 6, 7, 1, 3]
# print(quickSort(arr))
# arr = [9, 10, 8, 6, 7, 1, 3]
# print(mergeSort(arr))

plotTimeComplexity(quickSort, 500)