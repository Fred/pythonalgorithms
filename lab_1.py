def is_prime(n):
	'''
	>>> is_prime(7)
	True
	>>> is_prime(11)
	True
	>>> is_prime(4)
	False
	'''
	if (n < 2):
		return False
	for i in range(2, n):
		if n % i == 0:
			return False
	return True
def is_happy(n):
	'''
	>>> is_happy(19)
	True
	>>> is_happy(4)
	False
	'''
	tmp_set = set()
	while (n != 1 and n not in tmp_set):
		tmp_set.add(n)
		n = sum(int(x)**2 for x in str(n))
	return n == 1
def is_square(n):
	'''
	>>> is_square(1)
	True
	>>> is_square(4)
	True
	>>> is_square(121)
	True
	>>> is_square(35)
	False
	'''
	return (n ** 0.5 - int(n ** 0.5) == 0)
def is_triangular(n):
	'''
	>>> is_triangular(0)
	True
	>>> is_triangular(3)
	True
	>>> is_triangular(120)
	True
	>>> is_triangular(7)
	False
	'''
	tmp = 0
	counter = 1
	while (tmp < n):
		tmp += counter
		counter += 1
	return n == tmp

def is_smug(n):
	'''
	>>> is_smug(5)
	True
	>>> is_smug(13)
	True
	>>> is_smug(7)
	False
	'''
	smugNum1, smugNum2 = 1, 1
	while (n >= smugNum1 ** 2 + smugNum2 ** 2):
		if (n == smugNum1 ** 2 + smugNum2 ** 2):
			return True

		if (smugNum1 < smugNum2):
			smugNum1+=1
		elif (smugNum1 == smugNum2):
			smugNum2+=1
			if (smugNum1 != 1):
				smugNum1-=1
	return False
def is_honest(n):
	'''
	>>> is_honest(1)
	True
	>>> is_honest(3)
	True
	>>> is_honest(10)
	False
	>>> is_honest(17)
	False
	'''
	k = 1
	while (k <= n + 1):
		if ((n // k == k) and (k ** 2 != n)):
			return False
		k+=1
	return True

exercise = int(input("Введите номер задания: "))
num = int(input("Введите число: "))
if (exercise == 1):
	print("-- is_prime --")
	print(is_prime(num))
elif (exercise == 2):
	print("-- is_happy --")
	print(is_happy(num))
elif (exercise == 3):
	print("-- is_triangular --")
	print(is_triangular(num))
elif (exercise == 4):
	print("-- is_square --")
	print(is_square(num))
elif (exercise == 5):
	print("-- is_smug --")
	print(is_smug(num))
elif (exercise == 6):
	print("-- is_honest --")
	print(is_honest(num))
else:
	print("Такого задания нет.")