import time
emptyIrlist = ()
def make_irlist(first, rest = emptyIrlist):
	return (first, rest)

def irlist_head(irl):
	return irl[0]

def irlist_rest(irl):
	return irl[1]

def irlist_len(irlist, acc = 0):
	if (irlist == emptyIrlist):
		return acc
	else:
		return irlist_len(irlist_rest(irlist), acc + 1)

def irlist_select(irlist, index):
	if (index == 0):
		return irlist_head(irlist)
	else:
		return irlist_select(irlist_rest(irlist), index - 1)
def irlist_prepend(irlist, value):
	return make_irlist(value, irlist)
def irlist_append(irlist, value):
	if (irlist == emptyIrlist):
		return make_irlist(value)
	else:
		return irlist_append(irlist_rest(irlist), value)
		

ir = make_irlist(2)
ir = make_irlist(5, ir)
ir = make_irlist(1, ir)
print(ir)
print(irlist_select(ir, 2))
ir = irlist_append(ir, 6)
print(ir)



def setInterval(function, interval):
	timer = time.time()
	while(True):
		now = time.time()
		if (now - timer == interval):
			eval(function)
			timer = time.time()

# setInterval("print('Hello')", 1)
# print(list(filter(lambda x: x > 2, [1, 2, 5, 6, 2, 4, 6])))