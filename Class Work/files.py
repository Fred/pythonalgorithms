f = open('test.txt', 'r+') # r - read-only (default), w - write, a - append, r+ - read/write (b - binary (rb, wb, ab))

# f - TextIOWrapper
'''
text = f.read(1)
while (text != ''):
	print(text)
	text = f.read(1)

f.seek(0)
text = f.readline()
print(text)

f.seek(0)
text = f.readlines()
print(text)

f.seek(0)
for line in f:
	print(line)


f.seek(0)
for line in f:
	print(line)
'''
n = 0
while (n < 1000):
	f.write("qweqwe")
	f.seek(0)
	n+=1
	for line in f:
		print(line)
f.close()