# wins / (wins + losses) * 100%
# winsSwap / (winsSwap + lossesSwap) * 100%
from random import randint
import time

def gameInit(swap):
	global wins, losses, count
	game = setUpGame()
	personsDoor = 0
	game = swap(reveal(game, personsDoor, 1))

	if (game[personsDoor] == "car"):
		wins += 1
	else:
		losses += 1
	count += 1

def setUpGame():
	num = randint(0, 2)
	doors = ["goat"] * 3
	doors[num] = "car"

	return doors

def reveal(current, p_c, isBot = 0):
	num = randint(0, 2)
	while (num == p_c or current[num] == "car"):
		num = randint(0, 2)
	current[num] = "revealed"
	if (isBot == 0):
		print("Ведущий открыл дверь",  num + 1)
	return current	

def personsChoice(promt = ""):
	return int(input(promt)) - 1

def stats(wins, losses):
	return wins / (wins + losses) * 100

def botSwap(current):
	tmp = current[0]
	if (current[1] == "revealed"):
		# current[0], current[2] = current[2], current[0]
		current[0] = current[2]
		current[2] = tmp
	else:
		# current[0], current[1] = current[1], current[0]
		current[0] = current[1]
		current[1] = tmp
	return current

def doNothing(current):
	return current

print("3 двери. В одной - машина, в других - козлы.")
bot = input("Хотите чтобы бот сыграл за вас? (y | n): ")

wins = 0
losses = 0
if (bot == "y"):
	start = time.time()
	count = 0
	total = 100000
	while(count < total):
		toSwap = randint(0, 1) == 1 and botSwap or doNothing
		gameInit(toSwap)
	finish = time.time()
	print(" ")
	print("Время работы:", (finish - start))
else:
	while(True):
		game = setUpGame()
		personsDoor = personsChoice("Выберите дверь: ")
		game = reveal(game, personsDoor)
		print(" ")
		personsDoor = personsChoice("Хотите выбрать другую дверь? Если нет, введите ту же дверь: ")
		while (game[personsDoor] == "revealed"):
			personsDoor = personsChoice("Дверь открыта! Выберите другую дверь: ")
		if (game[personsDoor] == "car"):
			print("Поздравляю, вы победили!")
			wins += 1
		else:
			print("Вы проиграли. :c")
			losses += 1
		print(" ")
		exit = input("Хотите сыграть еще? (y | n): ")
		if (exit == "n"):
			break

print(" ")
print("Cпасибо за игру!")
print("Ваша статистика: ")
print("	Всего игр: ", wins + losses)
print("	Побед: ", wins)
print("	Поражений: ", losses)
print("	Процент побед: ", stats(wins, losses))
