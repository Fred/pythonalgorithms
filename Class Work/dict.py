import json
d = {'name': 'Filipp', 'lastName': 'Sher'}

f = open('dict.txt', 'w')
'''
for key, value in d.items():
	f.write(key + ":" + value + "\n")

del d

f.close()
f = open('dict.txt')
d = {}

for line in f:
	tmpList = line.split(":")
	d[tmpList[0]] = tmpList[1][:-1]

f.close()

for key, value in d.items():
	print(key + ":" + value)
'''

# Pickle - python3

json.dump(d, f)
f.close()

del d

f = open('dict.txt')
d = json.load(f)
print(d)